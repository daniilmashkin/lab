# -*- coding: utf-8 -*-
import scrapy
from lab.items import ReviewItem
import re


class KirovFlampReviewSpider(scrapy.Spider):
    # имя паука, используется при запуске
    # scrapy crawl kirov_flamp_review -o result/kirov_flamp_review.json -L INFO
    name = "delovoy_kirov_review"
    # массив первоначальных адресов
    start_urls = [
        "http://delovoy-kirov.ru/feed/reviews",
    ]

    # парсинг начинается именно с этого метода
    def parse(self, response):
        # чтобы видеть прогресс в решиме -L INFO выводим адрес руками
        self.logger.info(response.url)
        # получаем массив отзывов используя css селектор
        # селектор говорит о том что нужно выбрать все dom элементы типа ul с классом short-list
        reviews = response.css('div[class="review-content"]')
        if reviews:
            last_id = None
            # обходим все отзывы
            for review in reviews:
                # для каждого создаем объект ReviewItem
                review_item = ReviewItem()
                review_item['source_id'] = 2  # source_id для вашего сайто можно узнать в описании к лабе

                # используем xpath селекторы для извлечения нужных полей
                review_item['user_links'] = review.css('a::attr("href")').extract()
                review_item['user_name'] = review.css('a::text').extract()
                review_item['review_text'] = review.css('p::text').extract()
                yield scrapy.Request(
						review.css("div[class='item-header'] a::attr('href')").extract(),
						meta={
							'item':review_item,
							},
						callback=self.parse_company_page,
						dont_filter=True
                					)				

    
    def parse_company_page(self,response):
    	self.logger.info(response.url)
    	main_item=response.Request.meta['item']
    	main_item['company_name']=response.xpath("//h1[@class='title']/text()").extract()[0].encode('utf-8')
    	main_item['company_address']=response.xpath("//p[@class='module_address']/a/text()").extract()[0].encode('utf-8')
    	yield main_item
