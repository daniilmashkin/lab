# -*- coding: utf-8 -*-
import scrapy
from lab.items import ReviewItem
import re


class KirovKipovReviewSpider(scrapy.Spider):
    # имя паука, используется при запуске
    # scrapy crawl kirov_flamp_review -o result/kirov_flamp_review.json -L INFO
    name = "kirov_flamp_review"
    # массив первоначальных адресов
    start_urls = [
        "http://kirov.flamp.ru/feed?limit=10",
    ]

    # парсинг начинается именно с этого метода
    def parse(self, response):
        # чтобы видеть прогресс в решиме -L INFO выводим адрес руками
        self.logger.info(response.url)
        # получаем массив отзывов используя css селектор
        # селектор говорит о том что нужно выбрать все dom элементы типа ul с классом short-list
        reviews = response.css('ul.short-list')
        if reviews:
            last_id = None
            # обходим все отзывы
            for review in reviews:
                # для каждого создаем объект ReviewItem
                review_item = ReviewItem()
                review_item['source_id'] = 1  # source_id для вашего сайто можно узнать в описании к лабе

                # используем xpath селекторы для извлечения нужных полей
                review_item['review_link'] = review.xpath('li//meta[@itemprop="url"]/@content').extract()[0]
                review_item['review_rate_common'] = review.xpath('li//*[@itemprop="ratingValue"]/text()').extract()[0]
                review_item['review_text'] = review.xpath('li//*[@itemprop="reviewBody"]/text()').extract()[0]
                review_item['review_date'] = review.xpath('li//*[@itemprop="datePublished"]/@content').extract()[0]

                # сохраняем id последнего отзыва для перехода на другую страницу
                # это верно только для сайта flamp.ru
                # для других сайтов логика перехода на другую страницу может быть другой
                last_id = review.xpath('@data-id').extract()[0]

                # возвращаем генератор для следующего запроса на страницу юзера, который оставил отзыв
                # важно помнить что для каждого отзыва создается свой генератор
                # соотвественно для N отзывов на странице будет создано N дополнительных запросов
                # для получения информации о пользователях
                yield scrapy.Request(
                    # получаем ссылку на страницу юзера
                    review.xpath("li//a[@class='url user-name']/@href").extract()[0],
                    # передаем в параметрах запроса извлеченные данные об отзыве review_item
                    # и ссылку на организацию (т.к. на странице юзера этой ссылки не будет)
                    # параметр meta в scrapy.Request может быть произвольным словарем или отсутствовать вообще
                    meta={
                        'item': review_item,
                        'next': review.xpath("li//a[@class='link-short-list']/@href").extract()[0],
                    },
                    # метод, который будет использовать для парсинга страницы юзера
                    callback=self.parse_user_page,
                    # позволить scrapy повторно парсить эту страницу,
                    # чтобы обработать ситуацию, когда один пользователь оставил несколько отзывов
                    dont_filter=True
                )

            # обработка перехода на другую страницу с отзывами
            if 'from' in response.url:
                url = re.sub(r'from=\d+', 'from=%d' % int(last_id), response.url)
            else:
                url = response.url + ('&from=%d' % int(last_id))

            yield scrapy.Request(
                url,
                # повторно вызываем этот же метод, только для другой ссылки
                callback=self.parse
            )

    # метод для парсинга страницы пользователя
    def parse_user_page(self, response):
        # выводим ссылку в консоль
        self.logger.info(response.url)
        # берем созданный в предыдущем методе объект
        main_item = response.request.meta['item']

        # заполняем поля объекта с помощью xpath селекторов
        main_item['user_name'] = response.xpath("//*[@itemprop='name']/text()").extract()[0].encode('utf-8')
        main_item['user_links'] = response.xpath('//*[@id="f-col1-holder"]/div/div[2]/div[last()-1]/div/ul/li/a/@href').extract()

        # создаем запрос на обработку страницы организации, передаем обновленный объект
        yield scrapy.Request(
            response.request.meta['next'],
            meta={'item': main_item},
            # в качестве метода для обработки используем parse_company_page
            callback=self.parse_company_page,
            # позволяем scrapy многократно обрабатывать страницу оргназиаций,
            # чтобы обработать много отзывов об одной организации
            dont_filter=True
        )

    # метод для парсинга страницы организации
    def parse_company_page(self, response):
        self.logger.info(response.url)
        main_item = response.request.meta['item']

        main_item['company_name'] = response.xpath("//h1[@itemprop='name']/text()").extract()[0].encode('utf-8')
        main_item['company_phones'] = response.xpath("//meta[@itemprop='telephone']/@content").extract()
        main_item['company_region'] = response.xpath("//meta[@itemprop='addressLocality']/@content").extract()
        main_item['company_categories'] = response.css('ul.company-tags > li > a > h2::text').extract()
        main_item['company_links'] = {
            'vk': response.css("li.vkontakte> a::attr('href')").extract(),
            'twitter': response.css("li.twitter> a::attr('href')").extract(),
            'facebook': response.css("li.facebook> a::attr('href')").extract(),
            'instagram': response.css("li.instagram> a::attr('href')").extract(),
            'site': [
                re.search('\?(.*)', site).groups()[0]
                for site in
                response.xpath("//li[@class='website']/following-sibling::meta[1]/@content").extract()]
        }

        # На странице компании часто есть ссылка на все филиалы организации
        # необходимо сохранить все адреса в поле company_addresses
        affiliates_url = response.xpath('//a[re:test(@href, "/search/")]//@href').extract()
        if affiliates_url:
            # если есть ссылка на филиалы переходим на страницу филиалов и собираем все адреса
            yield scrapy.Request(response.urljoin(affiliates_url[0]), meta={'item': main_item},
                                 callback=self.parse_affiliates_page,
                                 dont_filter=True)
        else:
            main_item['company_addresses'] = response.xpath("//meta[@itemprop='streetAddress']/@content").extract()
            yield main_item
            return


    # парсинг страницы филиалов
    # заполняем последнее поле с адресами организации
    def parse_affiliates_page(self, response):
        main_item = response.request.meta['item']
        main_item['company_addresses'] = [
            a.strip()
            for a in
            response.css('div.company-location::text').extract()
            if a.strip() != u'']

        yield main_item
