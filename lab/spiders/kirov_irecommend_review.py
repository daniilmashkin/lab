# -*- coding: utf-8 -*-
import scrapy
from lab.items import ReviewItem
import re


class Kirov_irecommend_reviewSpider(scrapy.Spider):
    name = "kirov_irecommend_review"
    start_urls = [
        "http://irecommend.ru/taxonomy/term/30625",
    ]


    def parse(self, response):
        self.logger.info(response.url)
        links = response.css("div [class=citate] a::attr('href')").extract()
        if links:
            last_id = None

            for link in links:
                yield scrapy.Request(
                    review.xpath("li//a[@class='citate']/@href").extract()[0],
                    callback=self.parse_user_page,
                    dont_filter=True
                )

            
    def parse_user_page(self, response):
        self.logger.info(response.url)
        main_item = response.request.meta['item']

        main_item['company_name'] = response.xpath("//*[@itemprop='name']/text()").extract()[0].encode('utf-8')
        main_item['user_name'] = response.xpath("//*[@class='reviewer']/a/text()").extract()[0]
        main_item['user_link'] = response.xpath("//*[@class='reviewer']/a/@href").extract()
        main_item['review_date'] = 
        main_item['review_text'] = 