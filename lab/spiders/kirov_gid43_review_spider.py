# -*- coding: utf-8 -*-
import scrapy
from lab.items import ReviewItem
import re


class KirovGid43ReviewSpider(scrapy.Spider):
	# имя паука, используется при запуске
	# scrapy crawl kirov_flamp_review -o result/kirov_flamp_review.json -L INFO
	name = "kirov_gid43_review"
	# массив первоначальных адресов
	start_urls = [
		"http://www.gid43.ru/rubric/full/",
	]

	# парсинг начинается именно с этого метода
	def parse(self, response):
		# чтобы видеть прогресс в решиме -L INFO выводим адрес руками
		self.logger.info(response.url)
		# получаем массив отзывов используя css селектор
		# селектор говорит о том что нужно выбрать все dom элементы типа ul с классом short-list
		categories = response.xpath("//a[@class='rubrics-full-list-item-3-item']/@href").extract()
		for categorie in categories: 
			yield scrapy.Request(
				# получаем ссылку на страницу рубрики
				categorie,
				
				# метод, который будет использовать для парсинга страницы рубрики
				callback=self.parse_rubrics,
				# позволить scrapy повторно парсить эту страницу,
				# чтобы обработать ситуацию, когда один пользователь оставил несколько отзывов
				dont_filter=True
			)
			break

			
	# метод для парсинга страницы категории
	def parse_rubrics(self, response):
		# выводим ссылку в консоль
		self.logger.info(response.url)
		organizations = response.xpath("//a[@class='firm-item-title']/@href").extract()
		for organization in organizations:
			yield scrapy.Request(
				organization,
				meta={'rubric': response.xpath("//h1[@class='firms-list-title']/text()").extract()[0]},
				# в качестве метода для обработки используем parse_company_page
				callback=self.parse_organization_page,
				# позволяем scrapy многократно обрабатывать страницу оргназиаций,
				# чтобы обработать много отзывов об одной организации
				dont_filter=True
			)

	# метод для парсинга страницы организации
	def parse_organization_page(self, response):
		self.logger.info(response.url)
		rubric = response.request.meta['rubric']
		company_name = response.xpath("//h1/text()").extract()[0].encode('utf-8')
		company_phones = ""
		company_region = ""
		company_email = ""
		
		if len(response.xpath("//div[@class='votesRes']")) != 0:
			company_phones = response.xpath("//div[@class='phones']/text()").extract()[0]
			company_region = response.xpath("//div[@class='address']/text()").extract()[0]
			company_email = response.xpath("//div[@class='email']/text()").extract()[0]
		else:
			company_phones = response.xpath("//div[@class='firm-content-contact-info-phones']/text()").extract()[0]
			company_region = response.xpath("//div[@class='firm-content-contact-info-address-text']/text()").extract()[0]
			company_email = response.xpath("//div[@class='firm-content-contact-info-email']/text()").extract()[0]
		
		for review in response.xpath("//div[@class='comment-item']"):
			item = ReviewItem()
			item['rubric'] = rubric
			item['company_name'] = company_name
			item['company_phones'] = company_phones
			item['company_region'] = company_region
			item['company_email'] = company_email
			if review.xpath("a[@class='comment-item-user']"):
				item['user_name'] = review.xpath("a[@class='comment-item-user']/text()").extract()[0]
				item['user_link'] = review.xpath("a[@class='comment-item-user']/href").extract()
			else:
				item['user_name'] = review.xpath("span[@class='comment-item-user']/text()").extract()
			item['vote'] = review.xpath("div[@class='comment-item-rating-value']/@style").extract()
			item['date'] = review.xpath("div[@class='comment-item-date']/text()").extract()[0]
			item['text'] = review.xpath("div[@class='comment-item-text']/text()").extract()[0]

			yield item
		return