# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ReviewItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    source_id = scrapy.Field()

    company_name = scrapy.Field()
    company_addresses = scrapy.Field()
    company_region = scrapy.Field()
    company_categories = scrapy.Field()
    company_latitude = scrapy.Field()
    company_longitude = scrapy.Field()
    company_phones = scrapy.Field()
    company_links = scrapy.Field()

    user_name = scrapy.Field()
    user_links = scrapy.Field()

    review_date = scrapy.Field()
    review_text = scrapy.Field()
    review_link = scrapy.Field()
    review_rate_common = scrapy.Field()
    review_advantages = scrapy.Field()
    review_disadvantages = scrapy.Field()